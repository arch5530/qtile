#! /bin/bash
exec lxsession -a &
exec compton --config ~/.config/compton/compton.conf &
xfce4-power-manager &
feh --bg-fill ~/.config/wall.png &
nm-applet &
exec pulseaudio-X11 &
pasystray &
cbatticon &
