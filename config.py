
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import re
import socket
import subprocess
import platform
import sys
from libqtile.config import Key, Screen, Group, Drag, Click
from libqtile.lazy import lazy
from libqtile import layout, bar, widget
from libqtile import hook
from libqtile.config import Click, Drag

from typing import List  # noqa: F401

mod = "mod4"
myTerm = "kitty"
home = os.path.expanduser('~')
dmenu = "dmenu_run -c"
rofi = "rofi -show drun -show-icons"

mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
        start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
        start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]


keys = [
    # Switch between windows in current stack pane
    Key([mod], "k", lazy.layout.down()),
    Key([mod], "j", lazy.layout.up()),

    Key([mod], "h",
             lazy.layout.grow(),
             lazy.layout.increase_nmaster(),
             desc='Expand window (MonadTall), increase number in master pane (Tile)'
             ),
    Key([mod], "l",
             lazy.layout.shrink(),
             lazy.layout.decrease_nmaster(),
             desc='Shrink window (MonadTall), decrease number in master pane (Tile)'
             ),
    Key([mod], "n",
             lazy.layout.normalize(),
             desc='normalize window size ratios'
             ),

    # Move windows up or down in current stack
    Key([mod, "control"], "k", lazy.layout.shuffle_down()),
    Key([mod, "control"], "j", lazy.layout.shuffle_up()),

    # Switch window focus to other pane(s) of stack
    Key([mod], "space", lazy.layout.next()),

    # Swap panes of split stack
    Key([mod, "shift"], "space", lazy.layout.rotate()),

    #Keys to open programs
    Key([mod, "shift"], "Return", lazy.spawn(rofi)),
    Key([mod], "Return", lazy.spawn(myTerm)),
    Key([mod], "r", lazy.spawn(myTerm+ " -e ranger")),
    Key([mod], "c", lazy.spawn("chromium")),
    Key([mod], "f", lazy.spawn("pcmanfm")),
    Key([mod], "m", lazy.spawn("firefox")),
    Key([mod], "e", lazy.spawn("emacs")),
    Key([mod], "g", lazy.spawn("gimp")),
    Key([mod], "v", lazy.spawn("virtualbox")),
    Key([mod], "b", lazy.spawn("qutebrowser")),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout()),
    Key([mod], "q", lazy.window.kill()),

    Key([mod, "control"], "r", lazy.restart()),
    Key([mod, "control"], "q", lazy.shutdown()),

    # Media Keys
    Key([], "XF86AudioMute", lazy.spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ -10%")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ +10%")),
    # Screenshot
    Key([], "Print", lazy.spawn("/usr/bin/scrot " + home + "/Pictures/Screenshots/screenshot_%Y_%m_%d_%H_%M_%S.png")),

    ]

group_names = [("HOME", {'layout': 'monadtall'}),
               ("SCHOOL", {'layout': 'monadtall'}),
               ("STUDY",{'layout': 'monadtall'}),
               ("SYS", {'layout': 'monadtall'}),
               ("DOC", {'layout': 'monadtall'}),
               ("VBOX", {'layout': 'monadtall'}),
               ("WWW", {'layout': 'monadtall'}),
               ("MUSIC", {'layout': 'monadtall'})]

groups = [Group(name, **kwargs) for name, kwargs in group_names]

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([mod], str(i), lazy.group[name].toscreen()))        # Switch to another group
    keys.append(Key([mod, "shift"], str(i), lazy.window.togroup(name))) # Send current window to another group

    ##### DEFAULT THEME SETTINGS FOR LAYOUTS #####
layout_theme = {"border_width": 2,
                "margin": 3,
                "border_focus": "00246b",
                "border_normal": "2e333d"
                }


layouts = [
    # layout.Tile(**layout_theme),
    layout.MonadTall(**layout_theme),
    layout.Max(**layout_theme),
    layout.Floating(**layout_theme),
    # layout.Stack(num_stacks=2),
    # Try more layouts by unleashing below layouts.
    # layout.Bsp(**layout_theme),
    # layout.Columns(),
    # layout.Matrix(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]


##### COLORS #####
colors = [["#151c2c", "#151c2c"], # 0 panel background
          ["#bf616a", "#bf616a"], # 1
          ["#a3be8c", "#a3be8c"], # 2
          ["#ebcb8b", "#ebcb8b"], # 3 border line color for current tab
          ["#8fa1b3", "#8fa1b3"], # 4
          ["#b48ead", "#b48ead"], # 5
          ["#96b5b4", "#96b5b4"], # 6
          ["#e1acff", "#e1acff"], # 7
          ["#f0f0f0", "#f0f0f0"], # 8
          ["#c0c5ce", "#c0c5ce"], # 9
          ["#474747", "#474747"], # 10
          ["#000000", "#000000"]] # 11



#####WIDGET DEFAULT SETTINGS#####
widget_defaults = dict(
    font='Mononoki Nerd Font',
    fontsize=14,
    padding=3,
    background=colors[0]
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.Image(
                        filename= "~/.config/qtile/python.png",
                        background=colors[0],
                    ),
                widget.TextBox(
                            "|",
                            foreground=colors[1]
                        ),
                widget.CurrentLayout(),
                widget.CurrentLayoutIcon(scale=0.6),
                widget.TextBox(
                            "|",
                            foreground=colors[1]
                        ),
                widget.GroupBox(
                    font = "Ubuntu Bold",
                        fontsize = 12,
                        fontshadow=colors[11],
                        active=colors[8],
                        inactive=colors[10],
                        this_current_screen_border=colors[4],
                        borderwidth=1
                    ),
                widget.TextBox(
                            "|",
                            foreground=colors[1]
                        ),
                widget.Prompt(),
                widget.WindowName(),
                widget.Cmus(),
                widget.TextBox(
                            "|",
                            foreground=colors[1]
                        ),
                widget.TextBox("Updates:", name="default"),
                widget.Pacman(
                            execute = "termite",
                            update_interval = 100
                        ),
                widget.TextBox(
                            "|",
                            foreground=colors[1]
                        ),
                widget.CPUGraph(),
                widget.TextBox(
                            "|",
                            foreground=colors[1]
                        ),
                widget.Clock(
                        format='%A, %B %d %I:%M%p ',
                        ),
                widget.TextBox(
                            "|",
                            foreground=colors[1]
                        ),
                widget.Image(
                        filename= "~/.config/qtile/arch.png",
                        background=colors[0],
                        ),
                widget.TextBox("archlinux", name="default"),
                widget.TextBox(
                            "|",
                            foreground=colors[1]
                        ),
                widget.Systray(
                        icon_size=20,
                        ),
            ],
            24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = True
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
])
auto_fullscreen = True
focus_on_window_activation = "smart"

##### STARTUP APPLICATIONS #####
@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])
def runner():
    subprocess.Popen(['xsetroot', '-cursor_name', 'left_ptr'])

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "Qtile"
